FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

# Install required Ubuntu packages
RUN apt-get update  \
 && apt-get install -y --no-install-recommends \
    apt-transport-https \
    build-essential \
    ca-certificates \
    curl \
    dirmngr \
    git \
    gpg \
    gpg-agent \
    iputils-ping \
    iputils-tracepath \
    jq \
    libc-dev \
    libgmp-dev \
    libicu-dev \
    libpq-dev \
    libpq-dev \
    libtinfo-dev \
    locales \
    make \
    netbase \
    pkg-config \
    software-properties-common \
    tzdata \
    unzip \
    vim \
    zlib1g-dev \
 && apt-get clean  \
 && rm -rf /var/lib/apt/lists/*  \
 && jq --version


RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
RUN update-locale en_US.UTF-8

ENV HELM_VERSION=3.3.4
RUN curl -sSL "https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar xzf - -C /tmp && \
    mv /tmp/linux-amd64/helm /usr/local/bin/helm && \
    chmod +x /usr/local/bin/helm && \
    rm -rf /tmp/linux-amd64

ENV SOPS_VERSION=3.6.1
RUN curl -sSL "https://github.com/mozilla/sops/releases/download/v${SOPS_VERSION}/sops-v${SOPS_VERSION}.linux" -o /usr/local/bin/sops && \
    chmod +x /usr/local/bin/sops
